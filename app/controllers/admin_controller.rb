class AdminController < ApplicationController
  before_action :set_admin, only: [:show, :update, :destroy]
  #before_action :authenticate_user!
  #before_action :is_superadmin?

  def index
    @admins = CondoAdmin.all.joins(:user, :condominio).select('condo_admins.id, users.id as user_id, users.name, users.rut, users.email, condominios.address, condominios.namecondo, condominios.id as condominio_id').all.as_json

    render json: @admins
  end

  def show
    admin_id = params[:id]
    #@admin = CondoAdmin.where(user_id: admin_id).joins(:user, :condominio).where(user_id: admin_id).select('users.*, condominios.*').first
    @admin = CondoAdmin.where(user_id: admin_id).joins(:user, :condominio).where(user_id: admin_id).select('condo_admins.id, users.id as user_id, users.name, users.rut, users.email, condominios.id as condo_id, condominios.namecondo, condominios.address').first
    render json: @admin
  end

  def create

    # En front end tu tendrias algo como esto:
    # axios({
    #  url: '3r1312',
    #  method: 'post',
    #  params: {
    #     name: $('#name').val(),
    #     condominio_id: $('#peo').val(),
    #   }
    # })

    user = User.create!({
      :name => admin_params[:name],
      :email => admin_params[:email],
      :password => admin_params[:password],
      :user_type => 'admin',
      :password_confirmation => admin_params[:password_confirmation]
    })

    CondoAdmin.create!({
      user_id: user.id,
      condominio_id: admin_params[:condominio_id]
    })

    render status: 200, json: { :message => 'OK'}.to_json
  end

  def update
    if @admin.update(admin_params)
      render json: @admin
    else
      render json: @admin.errors, status: :unprocessable_entity
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin
      @admin = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def admin_params
      params.require(:admin).permit(:name, :password, :password_confirmation, :email, :condominio_id)
    end
end
