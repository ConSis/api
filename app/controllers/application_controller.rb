class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  def is_admin?
    if not current_user.nil? and current_user.user_type != 'admin'
      render json: {errors: ["Not authorized."]}, status: :unauthorized
    end
  end

  def is_copropietary?
    if not current_user.nil? and current_user.user_type != 'copropietary'
      render json: {errors: ["Not authorized."]}, status: :unauthorized
    end
  end

  def is_superadmin?
    if not current_user.nil? and current_user.user_type != 'adminSistema'
      render json: {errors: ["Not authorized."]}, status: :unauthorized
    end
  end

  #def is_admin_or_condo_admin?
  #def is_condo_admin?
  #  if not current_user.nil? and current_user.user_type != 'condo_admin'
  #    render json: {errors: ["Not authorized."]}, status: :unauthorized
  #  end
  #end
end


