class ComplainsController < ApplicationController
  before_action :set_complain, only: [:show, :update, :destroy]

  # GET /complains
  # GET /complains.json
  def index
    @complains = Complain.all
    render json: @complains
  end

  # GET /complains/1
  # GET /complains/1.json
  def show
    @complain = Complain.find(params[:id])
    render json: @complain
  end

  def complain_vote_up
    @complain = Complain.find(params[:id])
    @complain.votes = @complain.votes + 1
    @complain.save
    render json: @complain
  end

  def complain_vote_down
    @complain = Complain.find(params[:id])
    @complain.votes = @complain.votes - 1
    @complain.save
    render json: @complain
  end



  # POST /complains
  # POST /complains.json
  def create
    @complain = Complain.new(complain_params)

    if @complain.save
      render :show, status: :created, location: @complain
    else
      render json: @complain.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /complains/1
  # PATCH/PUT /complains/1.json
  def update
    if @complain.update(complain_params)
      render :show, status: :ok, location: @complain
    else
      render json: @complain.errors, status: :unprocessable_entity
    end
  end

  # DELETE /complains/1
  # DELETE /complains/1.json
  def destroy
    @complain.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_complain
      @complain = Complain.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def complain_params
      params.fetch(:complain, {})
    end
end
