class CondominiosController < ApplicationController
  before_action :set_condominio, only: [:show, :update, :destroy]
  #before_action :authenticate_user!
  #before_action :is_admin?, only: [:create, :destroy]
  before_action :is_superadmin?, only: [:create, :destroy]
  #before_action :is_copropietary?, only: [:show]


  ##
  # Lista todos los copropietarios según condominio.
  ##
  def get_copropietaries
    cond_id = params[:id]
    @cops = Copropietary.joins(:user).where(condominio_id: cond_id).select('users.*, copropietaries.*').as_json
    render json: @cops
  end

  def get_copropietaries_for_admin
    cond_id = params[:id]
    @cops = Copropietary.joins('LEFT JOIN houses ON houses.copropietary_id = copropietaries.id', :user).where(condominio_id: cond_id)

    resp = @cops.map do |copro|

      copro_object = {
        copropietary: {
          id: copro.id,
          user_id: copro.user_id,
          name: copro.user.name,
          rut: copro.user.rut,
          email: copro.user.email,
        },
      }

      house = copro.house

      if !house.nil?
        copro_object[:house] = {
          id: house.id,
          address: house.address,
          number: house.number,
          price: house.appartment_type.cost,
        }
      else
        copro_object[:house] = nil
      end

      parkings = copro.parkings.map do |parking|
        {
          name: parking.name,
          parking_type_name: parking.parking_type.name,
          parking_type_cost: parking.parking_type.cost,
        }
      end
      copro_object[:parkings] = parkings

      storages = copro.storages.map do |storage|
        {
          name: storage.name,
          storage_type_name: storage.storage_type.name,
          storage_type_cost: storage.storage_type.cost,
        }
      end
      copro_object[:storages] = storages
      copro_object
    end
    render json: resp
  end

  def create_payment
    cond_id  = params[:id]
    payment_params = register_payment_params

    payment = Payment.find_by(year: payment_params[:year], month: payment_params[:month])


    if payment.nil?
      payment = Payment.create!({
        :year => payment_params[:year],
        :month => payment_params[:month],
        :is_published => false,
        :condominio_id => cond_id
      })

      return render status: 200, json: { :message => 'OK', payment: payment }.as_json
    end

    return render status: 422, json: { :message => 'Ya existe pago para este mes y año'}.as_json
  end

  #para admin
  def get_not_published_payments
    cond_id = params[:id]
    @not_published_payments = Payment.where('payments.condominio_id = ?', cond_id).where('payments.is_published = ?', false).select('payments.id, payments.year, payments.month, payments.condominio_id as condominio_id')
    resp = @not_published_payments.map do |payment|
      object = {
        payment_not_published: {
          id: payment.id,
          year: payment.year,
          month: payment.month,
          cond_id: payment.condominio_id,
        }
      }

      @details = PaymentDetail.where('payment_details.payment_id = ?', payment.id)
      total_amount = 0
      @details.map do |detail|
        total_amount = total_amount + detail.amount
      end

      object[:total_amount] = {
        total_amount: total_amount
      }

      object
    end
  render json: resp
  end

  #para admin
  def get_published_payments
    cond_id = params[:id]
    @published_payments = Payment.where('payments.condominio_id = ?', cond_id).where('payments.is_published = ?', true).select('payments.id, payments.year, payments.month, payments.condominio_id as condominio_id')
    resp = @published_payments.map do |payment|
      object = {
        payment_published: {
          id: payment.id,
          year: payment.year,
          month: payment.month,
          cond_id: payment.condominio_id,
        }
      }

      @details = PaymentDetail.where('payment_details.payment_id = ?', payment.id)
      total_amount = 0
      @details.map do |detail|
        total_amount = total_amount + detail.amount
      end

      object[:total_amount] = {
        total_amount: total_amount
      }

      object
    end
  render json: resp
  end

  def register_payment_params
    params.require(:payment).permit(:year, :month)
  end

  def get_visits
    cond_id = params[:id]
    @visits = Copropietary.joins(:visits, :user).where('copropietaries.condominio_id = %d ' % cond_id).select('visits.id, visits.name, visits.rut, visits.plate, visits.created_at, visits.copropietary_id, users.name as copropietary_name').order('visits.created_at DESC').as_json;
    render json: @visits
  end

  def get_ads
    cond_id = params[:id]
    @ads = Copropietary.joins(:ads, :user).where('copropietaries.condominio_id = %d' % cond_id).select('ads.id, ads.title, ads.ad_category_id, ads.message, ads.is_approved, ads.copropietary_id as copro_id, users.name as copropietary_name').order('ads.created_at DESC').as_json;
    render json: @ads
  end

  def get_ad_categories
    cond_id = params[:id]
    @ad_categories = AdCategory.where('ad_categories.condominio_id = %d' % cond_id).select('ad_categories.id, ad_categories.name, ad_categories.condominio_id as condo_id').as_json;
    render json: @ad_categories
  end

  def get_complains
    cond_id = params[:id]
    @complains = Copropietary.joins(:complains, :user).where('copropietaries.condominio_id = %d' % cond_id).select('complains.id, complains.title, complains.message, complains.votes, complains.copropietary_id as copro_id, users.name as copropietary_name').order('complains.created_at DESC').as_json;
    render json: @complains
  end

  def get_all_packages
    cond_id = params[:id]
    @packages = Copropietary.joins(:packages, :user).where('copropietaries.condominio_id = %d' % cond_id).select('packages.id, packages.state, packages.arrived_date, packages.received_by_name, packages.description, packages.retrived_by_name, packages.retrived_date, packages.copropietary_id as copro_id, users.name as copro_name').order('packages.created_at DESC').as_json;
    render json: @packages
  end

  def get_news
    cond_id = params[:id]
    @news = New.where('news.condominio_id = %d' % cond_id).select('news.id, news.title, news.message, news.condominio_id as condo_id, news.created_at, news.updated_at').order('news.created_at DESC').as_json;
    render json: @news
  end

  def register_news
    cond_id = params.fetch(:id)
    news = register_news_params
    created_new = New.create(title: news[:title], message: news[:message], condominio_id: cond_id)
    render json: created_new.as_json
  end

  def register_news_params
    params.require(:new).permit(:title, :message)
  end

  def get_employees
    cond_id = params[:id]
    @employees = Employee.where('employees.condominio_id = %d' % cond_id).select('employees.id, employees.name, employees.rut, employees.email, employees.charge, employees.condominio_id as cond_id').as_json;
    render json: @employees
  end

  def register_employee
    cond_id = params.fetch(:id)
    employee = register_employee_params
    created_employee = Employee.create(name: employee[:name], rut: employee[:rut], email: employee[:email], charge: employee[:charge], condominio_id: cond_id)
    render json: created_employee.as_json
  end

  def register_employee_params
    params.require(:employee).permit(:name, :rut, :email, :charge)
  end

  def get_storage_types
    cond_id = params[:id]
    @storage_types = StorageType.where('storage_types.condominio_id = %d' % cond_id).select('storage_types.id, storage_types.name, storage_types.cost, storage_types.description, storage_types.condominio_id as condo_id').as_json;
    render json: @storage_types
  end

  def register_storage_type
    cond_id = params.fetch(:id)
    storage_type = register_storage_type_params
    created_storage_type = StorageType.create(name: storage_type[:name], cost: storage_type[:cost], description: storage_type[:description], condominio_id: cond_id)
    render json: created_storage_type
  end

  def register_storage_type_params
    params.require(:storage_type).permit(:name, :cost, :description)
  end

  def get_parking_types
    cond_id = params[:id]
    @parking_types = ParkingType.where('parking_types.condominio_id = %d' % cond_id).select('parking_types.id, parking_types.name, parking_types.cost, parking_types.description, parking_types.condominio_id as condo_id').as_json;
    render json: @parking_types
  end

  def register_parking_type
    cond_id = params.fetch(:id)
    parking_type = register_parking_type_params
    created_parking_type = ParkingType.create(name: parking_type[:name], cost: parking_type[:cost], description: parking_type[:description], condominio_id: cond_id)
    render json: created_parking_type
  end

  def register_parking_type_params
    params.require(:parking_type).permit(:name, :cost, :description)
  end

  def get_appartment_types
    cond_id = params[:id]
    @appartment_types = AppartmentType.where('appartment_types.condominio_id = %d' % cond_id).select('appartment_types.id, appartment_types.name, appartment_types.cost, appartment_types.description, appartment_types.condominio_id as condo_id').as_json;
    render json: @appartment_types
  end

  def register_appartment_type
    cond_id = params.fetch(:id)
    appartment_type = register_appartment_type_params
    created_appartemnt_type = AppartmentType.create(name: appartment_type[:name], cost: appartment_type[:cost], description: appartment_type[:description], condominio_id: cond_id)
    render json: created_appartemnt_type
  end

  def register_appartment_type_params
    params.require(:appartment_type).permit(:name, :cost, :description)
  end

  def get_transfer_info
    cond_id = params[:id]
    @transfer_info = TransferInfo.where(condominio_id: cond_id).select('transfer_infos.id, transfer_infos.name, transfer_infos.bank, transfer_infos.account_number, transfer_infos.rut, transfer_infos.account_type, transfer_infos.condominio_id as condo_id').first;
    render json: @transfer_info
  end

  def create_copropietary
    cond_id = params.fetch(:id)
    copropietary = register_copropietary_params
    created_user = User.create!({
      :name => copropietary[:name],
      :email => copropietary[:email],
      :password => 'asdasd123',
      :password_confirmation => 'asdasd123',
      :user_type => 'copropietary',
      :rut => copropietary[:rut]
    })

    created_copropietary = Copropietary.create!({
      :user_id => created_user.id,
      :condominio_id => cond_id
    })

    House.create!({
      :address => copropietary[:house_address],
      :number => copropietary[:house_number],
      :appartment_type_id => copropietary[:appartment_type_id],
      :copropietary_id => created_copropietary.id
    })

    parkings = register_parkings_copropietary_params
    parkings.each do |parking|
      created_parking = Parking.create!({
        :name => parking[:name],
        :parking_type_id => parking[:parking_type_id],
        :copropietary_id => created_copropietary.id
      })
    end

    storages = register_storages_copropietary_params
    storages.each do |storage|
      created_storage = Storage.create!({
        :name => storage[:name],
        :storage_type_id => storage[:storage_type_id],
        :copropietary_id => created_copropietary.id
      })

    end

    render status: 200, json: { :message => 'OK'}.to_json
  end

  def register_copropietary_params
    params.require(:copropietary).permit(:name, :rut, :email, :appartment_type_id, :house_address, :house_number)
  end

  def register_parkings_copropietary_params
    params.fetch(:parkings)
  end

  def register_storages_copropietary_params
    params.fetch(:storages)
  end

  ##
  # Obtiene el administrador según condominio
  ##
  #def get_admin
  #  admin_id = params[:id]
  #  @admin = CondoAdmin.joins(:condominio, :user).where(user_id: cond_id).select('condominios.*, condo_admins.*, users.*').as_json
  #  render json: @admin
  #end


  def get_admin
    cond_id = params[:id]
    @admin = CondoAdmin.joins(:user, :condominio).where(condominio_id: cond_id).select('users.*, condominios.*')
    render json: @admin
  end


  # GET /condominios
  def index
    @condominios = Condominio.all

    safe_params = params

    if (safe_params.has_key?(:without_admin) and safe_params[:without_admin])
      @condominios = Condominio.left_outer_joins(:condo_admin).where( condo_admins: { id: nil } )
    end

    render json: @condominios
  end

  # GET /condominios/1
  def show
    @condominio
  end

  # POST /condominios
  def create
    @condominio = Condominio.new(condominio_params)

    if @condominio.save
      render json: @condominio, status: :created, location: @condominio
    else
      render json: @condominio.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /condominios/1
  def update
    if @condominio.update(condominio_params)
      render json: @condominio
    else
      render json: @condominio.errors, status: :unprocessable_entity
    end
  end

  # DELETE /condominios/1
  def destroy
    @condominio.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_condominio
      @condominio = Condominio.find(params[:id])
    end

    def condominio_list_filters
      params.fetch(:filters).permit(:without_admin)
    end

    # Only allow a trusted parameter "white list" through.
    def condominio_params
      params.require(:condominio).permit(:namecondo, :address, :user_id)
    end
end
