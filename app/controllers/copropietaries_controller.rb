class CopropietariesController < ApplicationController

  def index
    @copropietary = Copropietary.all
    render json: @copropietary
  end

  #def show
  #  user_id = params[:id]
  #  @copropietary = Copropietary.where(user_id: user_id).joins(:user, :condominio).where(user_id: user_id).select('users.id as user_id, copropietaries.id as id, users.name as user_name, users.email as user_email, users.rut as user_rut, condominios.id as condo_id, condominios.namecondo as condo_name, condominios.address as condo_address').first
  #  render json: @copropietary
  #end

  def show
    user_id = params[:id]
    @copropietary = Copropietary.where(user_id: user_id).joins(:user, :condominio).where(user_id: user_id).select('users.id as user_id, copropietaries.id as id, users.name as user_name, users.email as user_email, users.rut as user_rut, condominios.id as condo_id, condominios.namecondo as condo_name, condominios.address as condo_address').first
    render json: @copropietary
  end

  def register_visits
  	user_id = params.fetch(:id)
    copropietary_id = User.find(user_id).copropietary.id
  	visits = register_visits_params

  	created_visits = []

  	visits.each do |visit|
  		p visit
  		created_visit = Visit.create(name: visit[:name], rut: visit[:rut], plate: visit[:plate], copropietary_id: copropietary_id, in_auto: visit[:plate] != nil)
  		created_visits.push(created_visit)
    end
  	render json: created_visits.as_json
  end

  def register_ad
    user_id = params.fetch(:id)
    copropietary_id = User.find(user_id).copropietary.id
    ad = register_ad_params
    created_ad = Ad.create(title: ad[:title], message: ad[:message], is_approved: false, ad_category_id: ad[:ad_category_id], copropietary_id: copropietary_id)
    render json: created_ad.as_json
  end

  def register_complaint
    user_id = params.fetch(:id)
    copropietary_id = User.find(user_id).copropietary.id
    complaint = register_complaint_params
    created_complaint = Complain.create(title: complaint[:title], message: complaint[:message], votes: 0, copropietary_id: copropietary_id)
    render json: created_complaint.as_json
  end

  def get_packages
    user_id = params[:id]
    copropietary_id = User.find(user_id).copropietary.id
    @packages = Package.where('packages.copropietary_id = %d' % copropietary_id).select('packages.id, packages.state, packages.arrived_date, packages.received_by_name, packages.retrived_date, packages.retrived_by_name, packages.description, packages.copropietary_id as copro_id').order('packages.created_at DESC').as_json
    render json: @packages
  end

  def register_package
    user_id = params.fetch(:id)
    copropietary_id = User.find(user_id).copropietary.id
    package = register_package_params
    created_package = Package.create(state: package[:state], arrived_date: package[:arrived_date], received_by_name: package[:received_by_name], retrived_date: package[:received_date], retrived_by_name: package[:retrived_by_name], description: package[:description], copropietary_id: copropietary_id)
    render json: created_package.as_json
  end

  def register_visits_params
  	params.fetch(:visits)
  end

  def register_ad_params
    params.require(:ad).permit(:title, :message, :is_approved, :ad_category_id)
  end

  def register_complaint_params
    params.require(:complaint).permit(:title, :message, :votes)
  end

  def register_package_params
    params.require(:package).permit(:state, :arrived_date, :received_by_name, :received_date, :description)
  end

  def get_user_payments
    copro_id = params[:id]
    @user_payments = UserPayment.where('user_payments.copropietary_id = ?', copro_id).where('user_payments.user_did_pay = 1 and user_payments.payment_confirmed = 1').select('user_payments.*')

    resp = @user_payments.map do |user_payment|
      object = {
        user_payment: {
          id: user_payment.id,
          user_did_pay: user_payment.user_did_pay,
          payment_confirmed: user_payment.payment_confirmed,
          year: user_payment.year,
          month: user_payment.month,
          total_amount: user_payment.total_amount,
          copropietary_id: user_payment.copropietary_id,
          payment_id: user_payment.payment_id,
        }
      }

      @details = PaymentDetail.where('payment_details.payment_id = ?', user_payment.payment_id)
      user_payment_details = @details.map do |detail|
        {
          description: detail.description,
          amount: detail.amount,
        }
      end
      object[:details] = user_payment_details
      object
    end
    render json: resp
  end

  def check_payment
    copro_id = params.fetch(:id)
    payment_params = check_payment_params
    payment = UserPayment.find_by(copropietary_id: copro_id, year: payment_params[:year], month: payment_params[:month])
    if payment.nil?
      return render status: 200, json: { :message => 'OK', payment: nil }.as_json
    else
      return render status: 200, json: { :message => 'OK', payment: payment }.as_json
    end
  end

  def get_appartment_type
    copro_id = params[:id]
    house = House.where('houses.copropietary_id = ?', copro_id).select('houses.id, houses.appartment_type_id as appartment_id, houses.address, houses.number')
    render json: house
  end

  def check_payment_params
    params.require(:payment).permit(:year, :month)
  end

end
