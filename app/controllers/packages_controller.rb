class PackagesController < ApplicationController
  before_action :set_package, only: [:show, :update, :destroy]

  # GET /packages
  # GET /packages.json
  def index
    @packages = Package.all
  end

  # GET /packages/1
  # GET /packages/1.json
  def show
  end

  # POST /packages
  # POST /packages.json
  def create
    @package = Package.new(package_params)

    if @package.save
      render :show, status: :created, location: @package
    else
      render json: @package.errors, status: :unprocessable_entity
    end
  end

  def change_state
    package_id = params.fetch(:id)
    package = change_state_package_params
    @package = Package.find(package_id)
    @package.state = 1
    @package.retrived_date = package[:retrived_date]
    @package.retrived_by_name = package[:retrived_by_name]
    @package.save
    render json: @package
  end

  # PATCH/PUT /packages/1
  # PATCH/PUT /packages/1.json
  def update
    if @package.update(package_params)
      render :show, status: :ok, location: @package
    else
      render json: @package.errors, status: :unprocessable_entity
    end
  end

  # DELETE /packages/1
  # DELETE /packages/1.json
  def destroy
    @package.destroy
  end

  def change_state_package_params
    params.require(:package).permit(:retrived_date, :retrived_by_name)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package
      @package = Package.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def package_params
      params.fetch(:package, {})
    end
end
