class ParkingTypesController < ApplicationController
  before_action :set_parking_type, only: [:show, :update, :destroy]

  # GET /parking_types
  # GET /parking_types.json
  def index
    @parking_types = ParkingType.all
  end

  # GET /parking_types/1
  # GET /parking_types/1.json
  def show
  end

  # POST /parking_types
  # POST /parking_types.json
  def create
    @parking_type = ParkingType.new(parking_type_params)

    if @parking_type.save
      render :show, status: :created, location: @parking_type
    else
      render json: @parking_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /parking_types/1
  # PATCH/PUT /parking_types/1.json
  def update
    if @parking_type.update(parking_type_params)
      render :show, status: :ok, location: @parking_type
    else
      render json: @parking_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /parking_types/1
  # DELETE /parking_types/1.json
  def destroy
    @parking_type.destroy
  end


  def destroy_parking_type
    @parking_type = ParkingType.find(params[:id])
    @parking_type.destroy
    render json: { status: "ok" }, status: 200
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parking_type
      @parking_type = ParkingType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def parking_type_params
      params.fetch(:parking_type, {})
    end
end
