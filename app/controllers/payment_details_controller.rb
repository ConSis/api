class PaymentDetailsController < ApplicationController
  before_action :set_payment_detail, only: [:show, :update, :destroy]

  # GET /payment_details
  # GET /payment_details.json
  def index
    @payment_details = PaymentDetail.all
  end

  # GET /payment_details/1
  # GET /payment_details/1.json
  def show
  end

  def get_payment_detail
    payment_detail_id = params[:id]
    @payment_detail = PaymentDetail.where('payment_details.id = ?', payment_detail_id).select('payment_details.id, payment_details.description, payment_details.amount, payment_details.payment_id as payment_id').first
    render json: @payment_detail
  end

  def edit_payment_detail
    payment_detail_id = params[:id]
    @payment_detail = PaymentDetail.find(payment_detail_id)
    payment_detail_params = edit_payment_detail_params
    @payment_detail.description = payment_detail_params[:description]
    @payment_detail.amount = payment_detail_params[:amount]
    @payment_detail.save
    render json: @payment_detail
  end

  def edit_payment_detail_params
    params.require(:payment_detail).permit(:description, :amount)
  end

  def destroy_payment_detail
    @payment_detail = PaymentDetail.find(params[:id])
    @payment_detail.destroy
    render json: { status: "ok" }, status: 200
  end

  # POST /payment_details
  # POST /payment_details.json
  def create
    @payment_detail = PaymentDetail.new(payment_detail_params)

    if @payment_detail.save
      render :show, status: :created, location: @payment_detail
    else
      render json: @payment_detail.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /payment_details/1
  # PATCH/PUT /payment_details/1.json
  def update
    if @payment_detail.update(payment_detail_params)
      render :show, status: :ok, location: @payment_detail
    else
      render json: @payment_detail.errors, status: :unprocessable_entity
    end
  end

  # DELETE /payment_details/1
  # DELETE /payment_details/1.json
  def destroy
    @payment_detail.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment_detail
      @payment_detail = PaymentDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_detail_params
      params.fetch(:payment_detail, {})
    end
end
