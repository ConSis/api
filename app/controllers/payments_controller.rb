class PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :update, :destroy]

  # GET /payments
  # GET /payments.json
  def index
    @payments = Payment.all
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = Payment.new(payment_params)

    if @payment.save
      render :show, status: :created, location: @payment
    else
      render json: @payment.errors, status: :unprocessable_entity
    end
  end

  def get_details
    payment_id = params[:id]
    payment = Payment.find(payment_id)
    total_copropietaries = Copropietary.where('copropietaries.condominio_id = ?', payment.condominio_id).length
    @details = PaymentDetail.where('payment_details.payment_id = ?', payment_id).select('payment_details.id, payment_details.description, payment_details.amount, payment_details.payment_id as payment_id')
    resp = @details.map do |detail|
      detail_object = {
       payment_detail: {
        id: detail.id,
        description: detail.description,
        amount: detail.amount,
        payment_id: detail.payment_id,
       },
      }

      total_amount_per_copro = detail.amount / total_copropietaries

      detail_object[:total_amount_per_copro] = {
        total: total_amount_per_copro,
      }
      detail_object
    end
    render json: resp
  end

  def create_detail
    payment_id = params.fetch(:id)
    detail_params = create_detail_params
    created_detail = PaymentDetail.create!({
      :description => detail_params[:description],
      :amount => detail_params[:amount],
      :payment_id => payment_id
    })

    return render status: 200, json: { :message => 'OK', payment: created_detail }.as_json
  end

  def create_detail_params
    params.require(:payment_detail).permit(:description, :amount)
  end

  def publish_payment
    payment_id = params.fetch(:id)
    payment_params = publish_payment_params
    # para dividir gastos comunes #
    total_copropietaries = Copropietary.where('copropietaries.condominio_id = ?', payment_params[:condominio_id]).length

    payment_bd = Payment.find(payment_id)
    payment_bd.is_published = true
    payment_bd.save

    ### GASTOS COMUNES AMOUNT #########
    @payment_details = PaymentDetail.where('payment_details.payment_id = ?', payment_bd.id)
    payment_details_amount = 0
    @payment_details.map do |payment|
      payment_details_amount = payment_details_amount + payment.amount
    end
    ## GASTO COMÚN POR COPROPIETARIO ##
    payment_details_amount_per_copro = payment_details_amount / total_copropietaries

    @copropietaries = Copropietary.where('copropietaries.condominio_id = ?', payment_params[:condominio_id])
    @copropietaries.map do |copropietary|
      #####CASA#########
      copropietary_house = House.where('houses.copropietary_id = ?', copropietary.id).first
      if !copropietary_house.nil?
        appartment_cost = AppartmentType.where('appartment_types.id = ?', copropietary_house.appartment_type_id).first.cost
      else
        appartment_cost = 0
      end

      ####ESTACIONAMIENTOS#####
      @parkings_copropietary = Parking.where('parkings.copropietary_id = ?', copropietary.id)
      total_parkings_amount = 0
      @parkings_copropietary.map do |parking|
        parking_cost = ParkingType.where('parking_types.id = ?', parking.parking_type_id).first.cost
        # en última iteración obtengo total de cobro estacionamiento
        total_parkings_amount = total_parkings_amount + parking_cost
      end
      ###########################

      #####BODEGAS#############
      @storages_copropietary = Storage.where('storages.copropietary_id = ?', copropietary.id)
      total_storages_amount = 0
      @storages_copropietary.map do |storage|
        storage_cost = StorageType.where('storage_types.id = ?', storage.storage_type_id).first.cost
        total_storages_amount = total_storages_amount + storage_cost
      end
      ###########################

      total_amount_per_copro = payment_details_amount_per_copro + appartment_cost + total_parkings_amount + total_storages_amount

      created_user_payment = UserPayment.create!({
        :user_did_pay => false,
        :payment_confirmed => false,
        :year => payment_bd.year,
        :month => payment_bd.month,
        :total_amount => total_amount_per_copro,
        :transfer_code => nil
        :copropietary_id => copropietary.id,
        :payment_id => payment_bd.id
      })
    end
    render status: 200, json: { :message => 'OK' }.to_json
  end

  def publish_payment_params
    params.require(:publish_payment).permit(:condominio_id)
  end

  def get_total_amount
    payment_id = params[:id]
    @payment_details = PaymentDetail.where('payment_details.payment_id = ?', payment_id)
    total_amount = 0
    @payment_details.map do |payment_detail|
      total_amount = total_amount + payment_detail.amount
    end

    render json: total_amount
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    if @payment.update(payment_params)
      render :show, status: :ok, location: @payment
    else
      render json: @payment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.fetch(:payment, {})
    end
end
