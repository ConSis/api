class StorageTypesController < ApplicationController
  before_action :set_storage_type, only: [:show, :update, :destroy]

  # GET /storage_types
  # GET /storage_types.json
  def index
    @storage_types = StorageType.all
  end

  # GET /storage_types/1
  # GET /storage_types/1.json
  def show
  end

  # POST /storage_types
  # POST /storage_types.json
  def create
    @storage_type = StorageType.new(storage_type_params)

    if @storage_type.save
      render :show, status: :created, location: @storage_type
    else
      render json: @storage_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /storage_types/1
  # PATCH/PUT /storage_types/1.json
  def update
    if @storage_type.update(storage_type_params)
      render :show, status: :ok, location: @storage_type
    else
      render json: @storage_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /storage_types/1
  # DELETE /storage_types/1.json
  def destroy
    @storage_type.destroy
  end

  def destroy_storage_type
    @storage_type = StorageType.find(params[:id])
    @storage_type.destroy
    render json: { status: "ok" }, status: 200
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_storage_type
      @storage_type = StorageType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def storage_type_params
      params.fetch(:storage_type, {})
    end
end
