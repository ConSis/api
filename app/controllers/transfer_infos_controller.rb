class TransferInfosController < ApplicationController
  before_action :set_transfer_info, only: [:show, :update, :destroy]
  #before_action :authenticate_user!
  # GET /transfer_infos
  def index
    @transfer_infos = TransferInfo.all

    render json: @transfer_infos
  end

  # GET /transfer_infos/1
  def show
    render json: @transfer_info
  end

  # POST /transfer_infos
  def create
    @transfer_info = TransferInfo.new(transfer_info_params)

    if @transfer_info.save
      render json: @transfer_info, status: :created, location: @transfer_info
    else
      render json: @transfer_info.errors, status: :unprocessable_entity
    end
  end

  def edit_transfer_info
    @transfer_info = TransferInfo.find(params[:id])
    transfer_info_data = edit_transfer_info_params
    @transfer_info.bank = transfer_info_data[:bank]
    @transfer_info.account_number = transfer_info_data[:account_number]
    @transfer_info.rut = transfer_info_data[:rut]
    @transfer_info.account_type = transfer_info_data[:account_type]
    @transfer_info.name = transfer_info_data[:name]
    @transfer_info.save
    render json: @transfer_info
  end

  def edit_transfer_info_params
    params.require(:transfer_info).permit(:bank, :account_number, :rut, :account_type, :name)
  end

  # PATCH/PUT /transfer_infos/1
  def update
    if @transfer_info.update(transfer_info_params)
      render json: @transfer_info
    else
      render json: @transfer_info.errors, status: :unprocessable_entity
    end
  end

  # DELETE /transfer_infos/1
  def destroy
    @transfer_info.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transfer_info
      @transfer_info = TransferInfo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def transfer_info_params
      params.fetch(:transfer_info, {})
    end
end
