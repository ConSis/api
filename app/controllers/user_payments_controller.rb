class UserPaymentsController < ApplicationController
  before_action :set_user_payment, only: [:show, :update, :destroy]

  # GET /user_payments
  # GET /user_payments.json
  def index
    @user_payments = UserPayment.all
  end

  # GET /user_payments/1
  # GET /user_payments/1.json
  def show
  end

  def get_user_payment
    user_payment_id = params[:id]
    user_payment = UserPayment.where('user_payments.id = ?', user_payment_id).select('user_payments.id, user_payments.user_did_pay, user_payments.payment_confirmed, user_payments.year, user_payments.month, user_payments.total_amount, user_payments.transfer_code, user_payments.copropietary_id as copro_id, user_payments.payment_id, payment_id').as_json;
    render json: user_payment
  end

  def change_state_user_payment
    user_payment_id = params.fetch(:id)
    params = change_state_payment_params
    user_payment_db = UserPayment.find(user_payment_id)
    user_payment_db.user_did_pay = true
    user_payment_db.transfer_code = params[:transfer_code]
    user_payment_db.save
    return render status: 200, json: { :message => 'OK', user_payment: user_payment_db }.as_json
  end

  def change_state_payment_params
    params.require(:user_payment).permit(:transfer_code)
  end

  # POST /user_payments
  # POST /user_payments.json
  def create
    @user_payment = UserPayment.new(user_payment_params)

    if @user_payment.save
      render :show, status: :created, location: @user_payment
    else
      render json: @user_payment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /user_payments/1
  # PATCH/PUT /user_payments/1.json
  def update
    if @user_payment.update(user_payment_params)
      render :show, status: :ok, location: @user_payment
    else
      render json: @user_payment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /user_payments/1
  # DELETE /user_payments/1.json
  def destroy
    @user_payment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_payment
      @user_payment = UserPayment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_payment_params
      params.fetch(:user_payment, {})
    end
end
