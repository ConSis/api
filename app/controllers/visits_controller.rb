class VisitsController < ApplicationController
  before_action :set_visit, only: [:show, :update, :destroy]

  # GET /visits
  def index
    @visits = Visit.all
  end

  # GET /visits/1
  def show
  end

  # POST /visits
  def create
    @visit = Visit.new(visit_params)

    if @visit.save
      render :show, status: :created, location: @visit
    else
      render json: @visit.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /visits/1
  def update
    if @visit.update(visit_params)
      render :show, status: :ok, location: @visit
    else
      render json: @visit.errors, status: :unprocessable_entity
    end
  end

  # DELETE /visits/1
  def destroy
    @visit.destroy
  end

  def destroy_visit
    @visit = Visit.find(params[:id])
    @visit.destroy
    render json: { status: "ok" }, status: 200
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_visit
      @visit = Visit.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def visit_params
      params.fetch(:visit, {})
    end
end
