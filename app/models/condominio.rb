class Condominio < ApplicationRecord
  has_one :transfer_infos
  has_one :condo_admin
  has_many :copropietaries
  has_many :news
  has_many :employees
  has_many :storage_types
  has_many :parking_types
  has_many :appartment_types
  has_many :payments
end
