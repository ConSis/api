class Copropietary < ApplicationRecord
  belongs_to :user
  belongs_to :condominio
  has_many :documents, dependent: :destroy
  has_many :visits
  has_many :ads
  has_many :complains
  has_many :packages
  has_many :storages
  has_many :parkings
  has_one :house
  has_many :user_payments
end
