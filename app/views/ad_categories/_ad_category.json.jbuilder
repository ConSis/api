json.extract! ad_category, :id, :created_at, :updated_at
json.url ad_category_url(ad_category, format: :json)
