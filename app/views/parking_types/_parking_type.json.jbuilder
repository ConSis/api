json.extract! parking_type, :id, :created_at, :updated_at
json.url parking_type_url(parking_type, format: :json)
