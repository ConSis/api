json.extract! storage_type, :id, :created_at, :updated_at
json.url storage_type_url(storage_type, format: :json)
