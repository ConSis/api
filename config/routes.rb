Rails.application.routes.draw do
  resources :condominios do
    resources :noticia
      resources :transfer_infos
  end
  resources :users do
    resources :documents
    resources :notifications
    resources :copropietaries
  end

  get 'condominios/:id/copropietaries', :to => 'condominios#get_copropietaries'
  get 'condominios/:id/copropietaries_for_admin', :to => 'condominios#get_copropietaries_for_admin'
  get 'condominios/:id/admin', :to => 'condominios#get_admin'
  get 'condominios/:id/visits', :to => 'condominios#get_visits'
  get 'condominios/:id/ads', :to => 'condominios#get_ads'
  get 'condominios/:id/ad_categories', :to => 'condominios#get_ad_categories'
  get 'condominios/:id/complaints', :to => 'condominios#get_complains'
  get 'condominios/:id/packages', :to => 'condominios#get_all_packages'
  get 'condominios/:id/news', :to => 'condominios#get_news'
  post 'condominios/:id/register_news', :to => 'condominios#register_news'
  get 'condominios/:id/employees', :to => 'condominios#get_employees'
  post 'condominios/:id/register_employee', :to => 'condominios#register_employee'
  get 'condominios/:id/storage_types', :to => 'condominios#get_storage_types'
  post 'condominios/:id/register_storage_type', :to => 'condominios#register_storage_type'
  get 'condominios/:id/parking_types', :to => 'condominios#get_parking_types'
  post 'condominios/:id/register_parking_type', :to => 'condominios#register_parking_type'
  get 'condominios/:id/appartment_types', :to => 'condominios#get_appartment_types'
  post 'condominios/:id/register_appartment_type', :to => 'condominios#register_appartment_type'
  get 'condominios/:id/transfer_info', :to => 'condominios#get_transfer_info'

  post 'condominios/:id/create_copropietary', :to => 'condominios#create_copropietary'
  post 'condominios/:id/create_payment', :to => 'condominios#create_payment'
  get 'condominios/:id/payments_not_published', :to => 'condominios#get_not_published_payments'
  get 'condominios/:id/payments_published', :to => 'condominios#get_published_payments'

  get 'payment/:id/details', :to => 'payments#get_details'
  post 'payment/:id/create_detail', :to => 'payments#create_detail'
  get 'payment/:id/amount', :to => 'payments#get_total_amount'

  post 'payment/:id/publish_payment', :to => 'payments#publish_payment'

  get 'payment_detail/:id/', :to => 'payment_details#get_payment_detail'
  post 'payment_detail/:id/edit', :to => 'payment_details#edit_payment_detail'
  post 'payment_detail/:id/remove', :to => 'payment_details#destroy_payment_detail'

  get 'ad_categories', :to => 'ad_categories#index'

  get 'admins/:id', :to => 'admin#show'
  get 'admins', :to => 'admin#index'
  post 'admins/create', :to => 'admin#create'


  get 'copropietaries', :to => 'copropietaries#index'
  get 'copropietary/:id', :to => 'copropietaries#show'

  get 'copropietary/:id/get_user_payments', :to => 'copropietaries#get_user_payments'
  post 'copropietary/:id/check_payment', :to => 'copropietaries#check_payment'
  post 'copropietary/:id/register_visits', :to => 'copropietaries#register_visits'
  post 'copropietary/:id/register_ad', :to => 'copropietaries#register_ad'
  post 'copropietary/:id/register_complaint', :to => 'copropietaries#register_complaint'
  post 'copropietary/:id/register_package', :to => 'copropietaries#register_package'
  get 'copropietary/:id/packages', :to => 'copropietaries#get_packages'
  get 'copropietary/:id/house', :to => 'copropietaries#get_appartment_type'

  get 'complains', :to => 'complains#index'
  get 'complains/:id', :to => 'complains#show'
  post 'complains/:id/vote_up', :to => 'complains#complain_vote_up'
  post 'complains/:id/vote_down', :to => 'complains#complain_vote_down'

  post 'package/:id/change_state', :to => 'packages#change_state'

  post 'news/:id/destroy_new', :to => 'news#destroy_news'

  post 'employee/:id/destroy_employee', :to => 'employees#destroy_employee'

  post 'visit/:id/destroy_visit', :to => 'visits#destroy_visit'

  post 'storage_type/:id/destroy_storage_type', :to => 'storage_types#destroy_storage_type'

  post 'parking_type/:id/destroy_parking_type', :to => 'parking_types#destroy_parking_type'

  post 'transfer_info/:id/edit', :to => 'transfer_infos#edit_transfer_info'

  get 'user_payment/:id', :to => 'user_payments#get_user_payment'
  post 'user_payment/:id/change_state', :to => 'user_payments#change_state_user_payment'

  mount_devise_token_auth_for 'User', at: 'auth'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
