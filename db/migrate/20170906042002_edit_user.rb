class EditUser < ActiveRecord::Migration[5.0]
  def change
    change_table(:users) do |t|
      t.remove :nickname
      t.column :username, :string, limit: 64
      t.remove :image
      t.column :rut, :string, limit: 64
      t.column :user_type, :string, limit: 64
    end
  end
end
