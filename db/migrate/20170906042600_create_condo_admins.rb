class CreateCondoAdmins < ActiveRecord::Migration[5.0]
  def change
    create_table :condo_admins do |t|
      t.references :user, foreign_key: true
      t.references :condominio, foreign_key: true

      t.timestamps
    end
  end
end
