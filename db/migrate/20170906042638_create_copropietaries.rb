class CreateCopropietaries < ActiveRecord::Migration[5.0]
  def change
    create_table :copropietaries do |t|
      t.references :user, foreign_key: true
      t.references :condominio, foreign_key: true
      t.datetime :payday
      t.string :avatar

      t.timestamps
    end
  end
end
