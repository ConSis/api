class CreateTransferInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :transfer_infos do |t|
      t.references :condominio, foreign_key: true
      t.string :bank
      t.string :account_number
      t.string :rut
      t.string :account_type
      t.string :name

      t.timestamps
    end
  end
end
