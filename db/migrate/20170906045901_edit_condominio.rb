class EditCondominio < ActiveRecord::Migration[5.0]
  def change
    change_table(:condominios) do |t|
      t.remove :user_id
    end
  end
end
