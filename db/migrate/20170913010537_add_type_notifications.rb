class AddTypeNotifications < ActiveRecord::Migration[5.0]
  def change
    change_table(:notifications) do |t|
      t.column :type, :string, limit: 64
    end
  end
end
