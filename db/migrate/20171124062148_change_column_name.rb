class ChangeColumnName < ActiveRecord::Migration[5.0]
  def change
    rename_column :condominios, :name, :name_condo
  end
end
