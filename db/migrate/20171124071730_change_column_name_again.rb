class ChangeColumnNameAgain < ActiveRecord::Migration[5.0]
  def change
    rename_column :condominios, :name_condo, :namecondo
  end
end
