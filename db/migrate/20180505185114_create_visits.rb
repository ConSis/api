class CreateVisits < ActiveRecord::Migration[5.0]
  def change
    create_table :visits do |t|
      t.string :name
      t.boolean :in_auto
      t.string :plate
      t.string :rut
      t.references :copropietary, foreign_key: true

      t.timestamps
    end
  end
end
