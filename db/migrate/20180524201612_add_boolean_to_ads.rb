class AddBooleanToAds < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :is_approved, :boolean
  end
end
