class AddReferenceToAd < ActiveRecord::Migration[5.0]
  def change
    add_reference :ads, :ad_category, foreign_key: true
  end
end
