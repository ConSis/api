class RemoveCondominioIdToAdCategories < ActiveRecord::Migration[5.0]
  def change
    remove_reference :ad_categories, :condominios, foreign_key: true
  end
end
