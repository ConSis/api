class AddCondominioIdToAdCategories < ActiveRecord::Migration[5.0]
  def change
    add_reference :ad_categories, :condominio, foreign_key: true
  end
end
