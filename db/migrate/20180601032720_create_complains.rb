class CreateComplains < ActiveRecord::Migration[5.0]
  def change
    create_table :complains do |t|
      t.string :title
      t.text :message
      t.integer :votes
      t.references :copropietary, foreign_key: true

      t.timestamps
    end
  end
end
