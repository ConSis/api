class CreatePackages < ActiveRecord::Migration[5.0]
  def change
    create_table :packages do |t|
      t.integer :state
      t.date :arrived_date
      t.string :received_by_name
      t.date :received_date
      t.text :description
      t.references :copropietary, foreign_key: true

      t.timestamps
    end
  end
end
