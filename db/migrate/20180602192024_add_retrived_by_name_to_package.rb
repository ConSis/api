class AddRetrivedByNameToPackage < ActiveRecord::Migration[5.0]
  def change
    add_column :packages, :retrived_by_name, :string
  end
end
