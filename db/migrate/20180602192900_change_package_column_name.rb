class ChangePackageColumnName < ActiveRecord::Migration[5.0]
  def change
  	rename_column :packages, :received_date, :retrived_date
  end
end
