class RemoveRetrivedDateToPackages < ActiveRecord::Migration[5.0]
  def change
    remove_column :packages, :retrived_date, :date
  end
end
