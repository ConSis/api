class CreateNews < ActiveRecord::Migration[5.0]
  def change
    create_table :news do |t|
      t.string :title
      t.text :message
      t.references :condominio, foreign_key: true

      t.timestamps
    end
  end
end
