class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :rut
      t.string :email
      t.string :charge
      t.references :condominio, foreign_key: true

      t.timestamps
    end
  end
end
