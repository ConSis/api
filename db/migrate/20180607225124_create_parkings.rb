class CreateParkings < ActiveRecord::Migration[5.0]
  def change
    create_table :parkings do |t|
      t.string :name
      t.references :parking_type, foreign_key: true

      t.timestamps
    end
  end
end
