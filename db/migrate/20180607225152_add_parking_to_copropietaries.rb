class AddParkingToCopropietaries < ActiveRecord::Migration[5.0]
  def change
    add_reference :copropietaries, :parking, foreign_key: true
  end
end
