class CreateHouses < ActiveRecord::Migration[5.0]
  def change
    create_table :houses do |t|
      t.string :name
      t.references :appartment_type, foreign_key: true
      t.string :address
      t.string :number

      t.timestamps
    end
  end
end
