class AddHouseToCopropietaries < ActiveRecord::Migration[5.0]
  def change
    add_reference :copropietaries, :house, foreign_key: true
  end
end
