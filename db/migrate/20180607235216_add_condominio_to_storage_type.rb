class AddCondominioToStorageType < ActiveRecord::Migration[5.0]
  def change
    add_reference :storage_types, :condominio, foreign_key: true
  end
end
