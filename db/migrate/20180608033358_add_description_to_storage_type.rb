class AddDescriptionToStorageType < ActiveRecord::Migration[5.0]
  def change
    add_column :storage_types, :description, :text
  end
end
