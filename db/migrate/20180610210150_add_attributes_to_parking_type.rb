class AddAttributesToParkingType < ActiveRecord::Migration[5.0]
  def change
    add_column :parking_types, :description, :text
    add_reference :parking_types, :condominio, foreign_key: true
  end
end
