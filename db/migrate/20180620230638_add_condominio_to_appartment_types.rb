class AddCondominioToAppartmentTypes < ActiveRecord::Migration[5.0]
  def change
    add_reference :appartment_types, :condominio, foreign_key: true
  end
end
