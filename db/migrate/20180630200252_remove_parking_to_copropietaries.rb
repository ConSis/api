class RemoveParkingToCopropietaries < ActiveRecord::Migration[5.0]
  def change
  	remove_column :copropietaries, :parking, :index
  end
end
