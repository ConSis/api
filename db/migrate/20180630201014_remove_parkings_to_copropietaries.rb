class RemoveParkingsToCopropietaries < ActiveRecord::Migration[5.0]
  def change
  	remove_foreign_key :copropietaries, :parkings
  end
end
