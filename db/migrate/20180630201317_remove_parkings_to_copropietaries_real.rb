class RemoveParkingsToCopropietariesReal < ActiveRecord::Migration[5.0]
  def change
  	remove_reference(:copropietaries, :parking, index: true)
  end
end
