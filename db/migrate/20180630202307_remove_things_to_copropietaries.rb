class RemoveThingsToCopropietaries < ActiveRecord::Migration[5.0]
  def change
  	remove_reference(:copropietaries, :storage, index: true)
  end
end
