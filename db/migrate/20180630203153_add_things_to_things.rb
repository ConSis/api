class AddThingsToThings < ActiveRecord::Migration[5.0]
  def change
  	add_reference(:houses, :copropietary, foreign_key: true)
  	add_reference(:storages, :copropietary, foreign_key: true)
  	add_reference(:parkings, :copropietary, foreign_key: true)
  end
end
