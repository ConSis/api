class RemoveHouseToCopropietary < ActiveRecord::Migration[5.0]
  def change
  	remove_reference(:copropietaries, :house, index: true)
  end
end
