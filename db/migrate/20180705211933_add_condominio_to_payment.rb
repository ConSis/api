class AddCondominioToPayment < ActiveRecord::Migration[5.0]
  def change
  	add_reference(:payments, :condominio, foreign_key: true)
  end
end
