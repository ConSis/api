class CreatePaymentDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_details do |t|
      t.text :description
      t.integer :amount
      t.references :payment, foreign_key: true

      t.timestamps
    end
  end
end
