class CreateUserPayments < ActiveRecord::Migration[5.0]
  def change
    create_table :user_payments do |t|
      t.boolean :user_did_pay
      t.boolean :payment_confirmed
      t.integer :year
      t.integer :month
      t.integer :total_amount
      t.references :copropietary, foreign_key: true
      t.references :payment, foreign_key: true

      t.timestamps
    end
  end
end
