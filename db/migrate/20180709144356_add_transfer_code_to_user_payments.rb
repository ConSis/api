class AddTransferCodeToUserPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :user_payments, :transfer_code, :string
  end
end
