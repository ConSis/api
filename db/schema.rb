# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180709144356) do

  create_table "ad_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "condominio_id"
    t.index ["condominio_id"], name: "index_ad_categories_on_condominio_id"
  end

  create_table "ads", force: :cascade do |t|
    t.string   "title"
    t.text     "message"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.boolean  "is_approved"
    t.integer  "ad_category_id"
    t.integer  "copropietary_id"
    t.index ["ad_category_id"], name: "index_ads_on_ad_category_id"
    t.index ["copropietary_id"], name: "index_ads_on_copropietary_id"
  end

  create_table "appartment_types", force: :cascade do |t|
    t.string   "name"
    t.integer  "cost"
    t.text     "description"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "condominio_id"
    t.index ["condominio_id"], name: "index_appartment_types_on_condominio_id"
  end

  create_table "appartments", force: :cascade do |t|
    t.integer  "condominio_id"
    t.string   "address"
    t.string   "label"
    t.string   "description"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["condominio_id"], name: "index_appartments_on_condominio_id"
  end

  create_table "complains", force: :cascade do |t|
    t.string   "title"
    t.text     "message"
    t.integer  "votes"
    t.integer  "copropietary_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["copropietary_id"], name: "index_complains_on_copropietary_id"
  end

  create_table "condo_admins", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "condominio_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["condominio_id"], name: "index_condo_admins_on_condominio_id"
    t.index ["user_id"], name: "index_condo_admins_on_user_id"
  end

  create_table "condominios", force: :cascade do |t|
    t.string   "namecondo"
    t.string   "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "copropietaries", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "condominio_id"
    t.datetime "payday"
    t.string   "avatar"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["condominio_id"], name: "index_copropietaries_on_condominio_id"
    t.index ["user_id"], name: "index_copropietaries_on_user_id"
  end

  create_table "documents", force: :cascade do |t|
    t.integer  "copropietary_id"
    t.string   "title"
    t.string   "data"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["copropietary_id"], name: "index_documents_on_copropietary_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string   "name"
    t.string   "rut"
    t.string   "email"
    t.string   "charge"
    t.integer  "condominio_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["condominio_id"], name: "index_employees_on_condominio_id"
  end

  create_table "houses", force: :cascade do |t|
    t.string   "name"
    t.integer  "appartment_type_id"
    t.string   "address"
    t.string   "number"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "copropietary_id"
    t.index ["appartment_type_id"], name: "index_houses_on_appartment_type_id"
    t.index ["copropietary_id"], name: "index_houses_on_copropietary_id"
  end

  create_table "news", force: :cascade do |t|
    t.string   "title"
    t.text     "message"
    t.integer  "condominio_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["condominio_id"], name: "index_news_on_condominio_id"
  end

  create_table "noticia", force: :cascade do |t|
    t.string   "title"
    t.string   "msge"
    t.integer  "condominio_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["condominio_id"], name: "index_noticia_on_condominio_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "title"
    t.string   "message"
    t.integer  "user_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "type",       limit: 64
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "packages", force: :cascade do |t|
    t.integer  "state"
    t.date     "arrived_date"
    t.string   "received_by_name"
    t.text     "description"
    t.integer  "copropietary_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "retrived_by_name"
    t.datetime "retrived_date"
    t.index ["copropietary_id"], name: "index_packages_on_copropietary_id"
  end

  create_table "parking_types", force: :cascade do |t|
    t.string   "name"
    t.integer  "cost"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.text     "description"
    t.integer  "condominio_id"
    t.index ["condominio_id"], name: "index_parking_types_on_condominio_id"
  end

  create_table "parkings", force: :cascade do |t|
    t.string   "name"
    t.integer  "parking_type_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "copropietary_id"
    t.index ["copropietary_id"], name: "index_parkings_on_copropietary_id"
    t.index ["parking_type_id"], name: "index_parkings_on_parking_type_id"
  end

  create_table "payment_details", force: :cascade do |t|
    t.text     "description"
    t.integer  "amount"
    t.integer  "payment_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["payment_id"], name: "index_payment_details_on_payment_id"
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "year"
    t.integer  "month"
    t.boolean  "is_published"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "condominio_id"
    t.index ["condominio_id"], name: "index_payments_on_condominio_id"
  end

  create_table "storage_types", force: :cascade do |t|
    t.string   "name"
    t.integer  "cost"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "condominio_id"
    t.text     "description"
    t.index ["condominio_id"], name: "index_storage_types_on_condominio_id"
  end

  create_table "storages", force: :cascade do |t|
    t.string   "name"
    t.integer  "storage_type_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "copropietary_id"
    t.index ["copropietary_id"], name: "index_storages_on_copropietary_id"
    t.index ["storage_type_id"], name: "index_storages_on_storage_type_id"
  end

  create_table "transfer_infos", force: :cascade do |t|
    t.integer  "condominio_id"
    t.string   "bank"
    t.string   "account_number"
    t.string   "rut"
    t.string   "account_type"
    t.string   "name"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["condominio_id"], name: "index_transfer_infos_on_condominio_id"
  end

  create_table "user_payments", force: :cascade do |t|
    t.boolean  "user_did_pay"
    t.boolean  "payment_confirmed"
    t.integer  "year"
    t.integer  "month"
    t.integer  "total_amount"
    t.integer  "copropietary_id"
    t.integer  "payment_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "transfer_code"
    t.index ["copropietary_id"], name: "index_user_payments_on_copropietary_id"
    t.index ["payment_id"], name: "index_user_payments_on_payment_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",                          default: "email", null: false
    t.string   "uid",                               default: "",      null: false
    t.string   "encrypted_password",                default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "email"
    t.text     "tokens"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "username",               limit: 64
    t.string   "rut",                    limit: 64
    t.string   "user_type",              limit: 64
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "visits", force: :cascade do |t|
    t.string   "name"
    t.boolean  "in_auto"
    t.string   "plate"
    t.string   "rut"
    t.integer  "copropietary_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["copropietary_id"], name: "index_visits_on_copropietary_id"
  end

end
