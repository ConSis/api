# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



#### Creación condominios:
#
#
Condominio.create!({:namecondo => "Condominio Zelda", :address => "Breath of the wild 123"})
Condominio.create!({:namecondo => "Condominio Mario", :address => "Galaxy 123"})
Condominio.create!({:namecondo => "Condominio Reprobar título", :address => "Rodrigo Torres 123"})
Condominio.create!({:namecondo => "Condominio Los Aromos", :address => "Francisco Bilbao 123"})
Condominio.create!({:namecondo => "Condominio Mandarina", :address => "Jacaranda 123"})
#
#
#### Creación de usuarios:
#
#
### Administrador sistema
User.create!({:email => "superadmin@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'adminSistema', :name => 'Super Admin', :username => 'superadmin', :rut => '85623125' })
#
### Administrador condominio
User.create!({:email => "admin@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'admin', :name => 'Rodrigo Torres', :username => 'adminRodrigo', :rut => '192891310' })
User.create!({:email => "admin2@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'admin', :name => 'Edison Torres', :username => 'adminEdison', :rut => '00000000' })
User.create!({:email => "admin3@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'admin', :name => 'Peo Torres', :username => 'adminPeo', :rut => '44444444' })

### Users tipo copropietario.
#
User.create!({:email => "copro@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'copropietary', :name => 'Ignacio León', :username => 'coproIgnacio', :rut => '94217067' })
User.create!({:email => "copro2@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'copropietary', :name => 'Felipe Morales', :username => 'coproFelipe', :rut => '167462146' })
User.create!({:email => "copro3@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'copropietary', :name => 'Leonardo Torres', :username => 'coproLeonardo', :rut => '111111111' })
User.create!({:email => "copro4@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'copropietary', :name => 'Benjamín Araneda', :username => 'coproBenja', :rut => '222222222' })
User.create!({:email => "copro5@consis.com", :password => "asdasd123", :password_confirmation => "asdasd123", :user_type => 'copropietary', :name => 'Fernanda Negrete', :username => 'coproFernanda', :rut => '333333333' })
#
#
### Copropietarios
#
CondoAdmin.create!({:user_id => 2, :condominio_id => 1})
CondoAdmin.create!({:user_id => 3, :condominio_id => 2})
CondoAdmin.create!({:user_id => 4, :condominio_id => 3})
Copropietary.create!({:user_id => 5, :condominio_id => 1})
Copropietary.create!({:user_id => 6, :condominio_id => 1})
Copropietary.create!({:user_id => 7, :condominio_id => 1})
Copropietary.create!({:user_id => 8, :condominio_id => 2})
Copropietary.create!({:user_id => 9, :condominio_id => 3})

#
#




