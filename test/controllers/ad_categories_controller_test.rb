require 'test_helper'

class AdCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ad_category = ad_categories(:one)
  end

  test "should get index" do
    get ad_categories_url, as: :json
    assert_response :success
  end

  test "should create ad_category" do
    assert_difference('AdCategory.count') do
      post ad_categories_url, params: { ad_category: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show ad_category" do
    get ad_category_url(@ad_category), as: :json
    assert_response :success
  end

  test "should update ad_category" do
    patch ad_category_url(@ad_category), params: { ad_category: {  } }, as: :json
    assert_response 200
  end

  test "should destroy ad_category" do
    assert_difference('AdCategory.count', -1) do
      delete ad_category_url(@ad_category), as: :json
    end

    assert_response 204
  end
end
