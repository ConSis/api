require 'test_helper'

class CondominiosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @condominio = condominios(:one)
  end

  test "should get index" do
    get condominios_url, as: :json
    assert_response :success
  end

  test "should create condominio" do
    assert_difference('Condominio.count') do
      post condominios_url, params: { condominio: { address: @condominio.address, name: @condominio.name, user_id: @condominio.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show condominio" do
    get condominio_url(@condominio), as: :json
    assert_response :success
  end

  test "should update condominio" do
    patch condominio_url(@condominio), params: { condominio: { address: @condominio.address, name: @condominio.name, user_id: @condominio.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy condominio" do
    assert_difference('Condominio.count', -1) do
      delete condominio_url(@condominio), as: :json
    end

    assert_response 204
  end
end
