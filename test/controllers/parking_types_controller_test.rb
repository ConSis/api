require 'test_helper'

class ParkingTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @parking_type = parking_types(:one)
  end

  test "should get index" do
    get parking_types_url, as: :json
    assert_response :success
  end

  test "should create parking_type" do
    assert_difference('ParkingType.count') do
      post parking_types_url, params: { parking_type: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show parking_type" do
    get parking_type_url(@parking_type), as: :json
    assert_response :success
  end

  test "should update parking_type" do
    patch parking_type_url(@parking_type), params: { parking_type: {  } }, as: :json
    assert_response 200
  end

  test "should destroy parking_type" do
    assert_difference('ParkingType.count', -1) do
      delete parking_type_url(@parking_type), as: :json
    end

    assert_response 204
  end
end
