require 'test_helper'

class StorageTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @storage_type = storage_types(:one)
  end

  test "should get index" do
    get storage_types_url, as: :json
    assert_response :success
  end

  test "should create storage_type" do
    assert_difference('StorageType.count') do
      post storage_types_url, params: { storage_type: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show storage_type" do
    get storage_type_url(@storage_type), as: :json
    assert_response :success
  end

  test "should update storage_type" do
    patch storage_type_url(@storage_type), params: { storage_type: {  } }, as: :json
    assert_response 200
  end

  test "should destroy storage_type" do
    assert_difference('StorageType.count', -1) do
      delete storage_type_url(@storage_type), as: :json
    end

    assert_response 204
  end
end
