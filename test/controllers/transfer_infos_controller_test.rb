require 'test_helper'

class TransferInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @transfer_info = transfer_infos(:one)
  end

  test "should get index" do
    get transfer_infos_url, as: :json
    assert_response :success
  end

  test "should create transfer_info" do
    assert_difference('TransferInfo.count') do
      post transfer_infos_url, params: { transfer_info: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show transfer_info" do
    get transfer_info_url(@transfer_info), as: :json
    assert_response :success
  end

  test "should update transfer_info" do
    patch transfer_info_url(@transfer_info), params: { transfer_info: {  } }, as: :json
    assert_response 200
  end

  test "should destroy transfer_info" do
    assert_difference('TransferInfo.count', -1) do
      delete transfer_info_url(@transfer_info), as: :json
    end

    assert_response 204
  end
end
