require 'test_helper'

class UserPaymentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_payment = user_payments(:one)
  end

  test "should get index" do
    get user_payments_url, as: :json
    assert_response :success
  end

  test "should create user_payment" do
    assert_difference('UserPayment.count') do
      post user_payments_url, params: { user_payment: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show user_payment" do
    get user_payment_url(@user_payment), as: :json
    assert_response :success
  end

  test "should update user_payment" do
    patch user_payment_url(@user_payment), params: { user_payment: {  } }, as: :json
    assert_response 200
  end

  test "should destroy user_payment" do
    assert_difference('UserPayment.count', -1) do
      delete user_payment_url(@user_payment), as: :json
    end

    assert_response 204
  end
end
